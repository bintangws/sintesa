<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Image,Validator,Input;
use App\User;
use App\Role;

class AdminController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function index()
  {
    $users = User::all();
    // echo "<pre>";print_r($users);die;
    return view('administrator.index',compact('users'));
  }

  public function read($id)
  {
    $users = User::find($id);
    return view('administrator.read',compact('users'));
  }

  public function edit($id)
  {
    $users = User::find($id);
    // echo "<pre>";print_r($users->birthday);die;
    return view('administrator.edit',compact('users'));
  }

  public function create()
  {
    return view('administrator.create');
  }

  public function store(Request $request)
  {
    $users = new User;
    // Get user role by name
    // ===============================================
    $get_role_by_name = Role::where('name','administrator')->first();
    $assignToAdmin = $get_role_by_name->id;
    // =========================================
    // Validator Class Must be Input as Above and Below
    $validator = Validator::make($request->all(), [
      'name' => 'required',
      'email' => 'required',
      'password' => 'min:6|required',
      'password_confirmation' => 'min:6|same:password'
    ]);

    if ($validator->fails()) {
      return redirect('admin/add-user')
        ->withErrors($validator)
        ->withInput();
    } else {
      $users->name = $request->input('name');
      $users->email = $request->input('email');
      $users->password = bcrypt($request->input('password'));
      // echo "<pre>";print_r($users);die;
      $users->save();
      $users->attachRole($assignToAdmin);
    }
    $request->session()->flash('status', 'Data Has Been Inserted');
    return redirect('admin/admin-management');
  }

  public function update(Request $request,$id)
  {
    $users = User::find($id);
    if ($request->input('password')) {
        $validator = Validator::make($request->all(), [
          'password' => 'min:6|required',
          'password_confirmation' => 'min:6|same:password'
        ]);

        if ($validator->fails()) {
          return redirect('admin/add-user')
            ->withErrors($validator)
            ->withInput();
        } else {
          $users->name = $request->input('name');
          $users->email = $request->input('email');
          $users->password = bcrypt($request->input('password'));
        }
    } else {
        $users->name = $request->input('name');
        $users->email = $request->input('email');
    }
    $users->save();
    $request->session()->flash('status', 'Data Has Been updated');
    return redirect('admin/admin-management');
  }

  public function delete($id)
  {
    $users = User::find($id);
    $users->delete();
    return redirect('admin/admin-management')->with('status','Data Has Been Deleted');
  }
}
