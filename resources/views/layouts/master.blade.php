<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap admin template">
    <meta name="author" content="">
    <title>Admin Dashboard</title>
    <link rel="apple-touch-icon" href="{{url('assets/images/apple-touch-icon.png')}}">
    <link rel="shortcut icon" href="{{url('assets/images/favicon.ico')}}">
    <!-- Stylesheets -->
    <link rel="stylesheet" href="{{url('global/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('global/css/bootstrap-extend.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/site.min.css')}}">
    <!-- Plugins -->
    <link rel="stylesheet" href="{{url('global/vendor/animsition/animsition.css')}}">
    <link rel="stylesheet" href="{{url('global/vendor/asscrollable/asScrollable.css')}}">
    <link rel="stylesheet" href="{{url('global/vendor/switchery/switchery.css')}}">
    <link rel="stylesheet" href="{{url('global/vendor/intro-js/introjs.css')}}">
    <link rel="stylesheet" href="{{url('global/vendor/slidepanel/slidePanel.css')}}">
    <link rel="stylesheet" href="{{url('global/vendor/flag-icon-css/flag-icon.css')}}">
    <link rel="stylesheet" href="{{url('global/vendor/waves/waves.css')}}">
    <!-- <link rel="stylesheet" href="{{url('global/vendor/bootstrap-markdown/bootstrap-markdown.css')}}">
     -->
    <!-- <link rel="stylesheet" href="{{url('global/vendor/chartist-js/chartist.css')}}">
    <link rel="stylesheet" href="{{url('global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css')}}"> -->

    <!-- DatePicker -->
    <link rel="stylesheet" href="{{url('global/vendor/select2/select2.css')}}">
    <link rel="stylesheet" href="{{url('global/vendor/bootstrap-tokenfield/bootstrap-tokenfield.css')}}">
    <link rel="stylesheet" href="{{url('global/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css')}}">
    <link rel="stylesheet" href="{{url('global/vendor/bootstrap-select/bootstrap-select.css')}}">
    <link rel="stylesheet" href="{{url('global/vendor/icheck/icheck.css')}}">
    <link rel="stylesheet" href="{{url('global/vendor/switchery/switchery.css')}}">
    <link rel="stylesheet" href="{{url('global/vendor/asrange/asRange.css')}}">
    <link rel="stylesheet" href="{{url('global/vendor/asspinner/asSpinner.css')}}">
    <link rel="stylesheet" href="{{url('global/vendor/clockpicker/clockpicker.css')}}">
    <link rel="stylesheet" href="{{url('global/vendor/ascolorpicker/asColorPicker.css')}}">
    <link rel="stylesheet" href="{{url('global/vendor/bootstrap-touchspin/bootstrap-touchspin.css')}}">
    <link rel="stylesheet" href="{{url('global/vendor/card/card.css')}}">
    <link rel="stylesheet" href="{{url('global/vendor/jquery-labelauty/jquery-labelauty.css')}}">
    <link rel="stylesheet" href="{{url('global/vendor/bootstrap-datepicker/bootstrap-datepicker.css')}}">
    <link rel="stylesheet" href="{{url('global/vendor/bootstrap-maxlength/bootstrap-maxlength.css')}}">
    <link rel="stylesheet" href="{{url('global/vendor/jt-timepicker/jquery-timepicker.css')}}">
    <link rel="stylesheet" href="{{url('global/vendor/jquery-strength/jquery-strength.css')}}">
    <link rel="stylesheet" href="{{url('global/vendor/multi-select/multi-select.css')}}">
    <link rel="stylesheet" href="{{url('global/vendor/typeahead-js/typeahead.css')}}">
    <link rel="stylesheet" href="{{url('assets/examples/css/forms/advanced.css')}}">
    <link rel="stylesheet" href="{{url('global/vendor/datatables-bootstrap/dataTables.bootstrap.css')}}">

    <!-- Fonts -->
    <link rel="stylesheet" href="{{url('global/fonts/font-awesome/font-awesome.css')}}">
    <link rel="stylesheet" href="{{url('global/fonts/material-design/material-design.min.css')}}">
    <link rel="stylesheet" href="{{url('global/fonts/brand-icons/brand-icons.min.css')}}">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
    <!--[if lt IE 9]>
      <script src="{{url('adminglobal/vendor/html5shiv/html5shiv.min.js')}}"></script>
      <![endif]-->
    <!--[if lt IE 10]>
      <script src="{{url('adminglobal/vendor/media-match/media.match.min.js')}}"></script>
      <script src="{{url('adminglobal/vendor/respond/respond.min.js')}}"></script>
      <![endif]-->
    <!-- Scripts -->
    <script src="{{url('global/vendor/modernizr/modernizr.js')}}"></script>
    <script src="{{url('global/vendor/breakpoints/breakpoints.js')}}"></script>
    <script>
    Breakpoints();
    </script>
  </head>

  <body class="dashboard">
    <nav class="site-navbar navbar navbar-inverse navbar-fixed-top navbar-mega" role="navigation">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle hamburger hamburger-close navbar-toggle-left hided"
          data-toggle="menubar">
            <span class="sr-only">Toggle navigation</span>
            <span class="hamburger-bar"></span>
          </button>
          <button type="button" class="navbar-toggle collapsed" data-target="#site-navbar-collapse"
          data-toggle="collapse">
            <i class="icon md-more" aria-hidden="true"></i>
          </button>
          <div class="navbar-brand navbar-brand-center site-gridmenu-toggle" data-toggle="gridmenu">
            <img class="navbar-brand-logo" src="{{url('assets/images/logo.png')}}" title="Remark">
            <span class="navbar-brand-text hidden-xs"> Remark</span>
          </div>
          <button type="button" class="navbar-toggle collapsed" data-target="#site-navbar-search"
          data-toggle="collapse">
            <span class="sr-only">Toggle Search</span>
            <i class="icon md-search" aria-hidden="true"></i>
          </button>
        </div>
        <div class="navbar-container container-fluid">
          <!-- Navbar Collapse -->
          <div class="collapse navbar-collapse navbar-collapse-toolbar" id="site-navbar-collapse">
            <!-- Navbar Toolbar Right -->
            <ul class="nav navbar-toolbar navbar-right navbar-toolbar-right">
              <li class="dropdown">
                <a data-toggle="dropdown" href="javascript:void(0)" title="Notifications" aria-expanded="false"
                data-animation="scale-up" role="button">
                  <i class="icon md-notifications" aria-hidden="true"></i>
                  <span class="badge badge-danger up">5</span>
                </a>
                <ul class="dropdown-menu dropdown-menu-right dropdown-menu-media" role="menu">
                  <li class="dropdown-menu-header" role="presentation">
                    <h5>NOTIFICATIONS</h5>
                    <span class="label label-round label-danger">New 5</span>
                  </li>
                  <li class="list-group" role="presentation">
                    <div data-role="container">
                      <div data-role="content">
                        <a class="list-group-item" href="javascript:void(0)" role="menuitem">
                          <div class="media">
                            <div class="media-left padding-right-10">
                              <i class="icon md-receipt bg-red-600 white icon-circle" aria-hidden="true"></i>
                            </div>
                            <div class="media-body">
                              <h6 class="media-heading">A new order has been placed</h6>
                              <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">5 hours ago</time>
                            </div>
                          </div>
                        </a>
                        <a class="list-group-item" href="javascript:void(0)" role="menuitem">
                          <div class="media">
                            <div class="media-left padding-right-10">
                              <i class="icon md-account bg-green-600 white icon-circle" aria-hidden="true"></i>
                            </div>
                            <div class="media-body">
                              <h6 class="media-heading">Completed the task</h6>
                              <time class="media-meta" datetime="2015-06-11T18:29:20+08:00">2 days ago</time>
                            </div>
                          </div>
                        </a>
                        <a class="list-group-item" href="javascript:void(0)" role="menuitem">
                          <div class="media">
                            <div class="media-left padding-right-10">
                              <i class="icon md-settings bg-red-600 white icon-circle" aria-hidden="true"></i>
                            </div>
                            <div class="media-body">
                              <h6 class="media-heading">Settings updated</h6>
                              <time class="media-meta" datetime="2015-06-11T14:05:00+08:00">2 days ago</time>
                            </div>
                          </div>
                        </a>
                        <a class="list-group-item" href="javascript:void(0)" role="menuitem">
                          <div class="media">
                            <div class="media-left padding-right-10">
                              <i class="icon md-calendar bg-blue-600 white icon-circle" aria-hidden="true"></i>
                            </div>
                            <div class="media-body">
                              <h6 class="media-heading">Event started</h6>
                              <time class="media-meta" datetime="2015-06-10T13:50:18+08:00">3 days ago</time>
                            </div>
                          </div>
                        </a>
                        <a class="list-group-item" href="javascript:void(0)" role="menuitem">
                          <div class="media">
                            <div class="media-left padding-right-10">
                              <i class="icon md-comment bg-orange-600 white icon-circle" aria-hidden="true"></i>
                            </div>
                            <div class="media-body">
                              <h6 class="media-heading">Message received</h6>
                              <time class="media-meta" datetime="2015-06-10T12:34:48+08:00">3 days ago</time>
                            </div>
                          </div>
                        </a>
                      </div>
                    </div>
                  </li>
                  <li class="dropdown-menu-footer" role="presentation">
                    <a class="dropdown-menu-footer-btn" href="javascript:void(0)" role="button">
                      <i class="icon md-settings" aria-hidden="true"></i>
                    </a>
                    <a href="javascript:void(0)" role="menuitem">
                        All notifications
                      </a>
                  </li>
                </ul>
              </li>
              <li class="dropdown">
                <a class="navbar-avatar dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false"
                data-animation="scale-up" role="button">
                  <span class="avatar avatar-online">
                    <img src="{{url('global/portraits/5.jpg')}}" alt="...">
                    <i></i>
                  </span>
                </a>
                <ul class="dropdown-menu" role="menu">
                  <li role="presentation">
                    <a href="javascript:void(0)" role="menuitem"><i class="icon md-account" aria-hidden="true"></i> Profile</a>
                  </li>
                  <li role="presentation">
                    <a href="javascript:void(0)" role="menuitem"><i class="icon md-card" aria-hidden="true"></i> Billing</a>
                  </li>
                  <li role="presentation">
                    <a href="javascript:void(0)" role="menuitem"><i class="icon md-settings" aria-hidden="true"></i> Settings</a>
                  </li>
                  <li class="divider" role="presentation"></li>
                  <li role="presentation">
                    <a href="javascript:void(0)" role="menuitem"><i class="icon md-power" aria-hidden="true"></i> Logout</a>
                  </li>
                </ul>
              </li>
            </ul>
            <!-- End Navbar Toolbar Right -->
          </div>
          <!-- End Navbar Collapse -->
          <!-- Site Navbar Seach -->
          <div class="collapse navbar-search-overlap" id="site-navbar-search">
            <form role="search">
              <div class="form-group">
                <div class="input-search">
                  <i class="input-search-icon md-search" aria-hidden="true"></i>
                  <input type="text" class="form-control" name="site-search" placeholder="Search...">
                  <button type="button" class="input-search-close icon md-close" data-target="#site-navbar-search"
                  data-toggle="collapse" aria-label="Close"></button>
                </div>
              </div>
            </form>
          </div>
          <!-- End Site Navbar Seach -->
        </div>
    </nav>
  @include('layouts.sidebar')
  <!-- Page -->
    <div class="page animsition">
      @yield('content-backend')
    </div>



    <!-- Footer -->
    <footer class="site-footer">
      <div class="site-footer-legal">© 2018 <a href="#">Zenit</a></div>
      <div class="site-footer-right">
        Crafted with <i class="red-600 icon md-favorite"></i> by <a href="#">Bintang Widiyo S</a>
      </div>
    </footer>
    <!-- Core  -->
    <script src="{{url('global/vendor/jquery/jquery.js')}}"></script>
    <script src="{{url('global/vendor/bootstrap/bootstrap.js')}}"></script>
    <script src="{{url('global/vendor/animsition/animsition.js')}}"></script>
    <script src="{{url('global/vendor/asscroll/jquery-asScroll.js')}}"></script>
    <script src="{{url('global/vendor/mousewheel/jquery.mousewheel.js')}}"></script>
    <script src="{{url('global/vendor/asscrollable/jquery.asScrollable.all.js')}}"></script>
    <script src="{{url('global/vendor/ashoverscroll/jquery-asHoverScroll.js')}}"></script>
    <script src="{{url('global/vendor/waves/waves.js')}}"></script>
    <!-- Plugins -->
    <script src="{{url('global/vendor/switchery/switchery.min.js')}}"></script>
    <script src="{{url('global/vendor/intro-js/intro.js')}}"></script>
    <script src="{{url('global/vendor/screenfull/screenfull.js')}}"></script>
    <script src="{{url('global/vendor/slidepanel/jquery-slidePanel.js')}}"></script>
    <script src="{{url('global/vendor/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{url('global/vendor/datatables-fixedheader/dataTables.fixedHeader.js')}}"></script>
    <script src="{{url('global/vendor/datatables-bootstrap/dataTables.bootstrap.js')}}"></script>
    <script src="{{url('global/vendor/datatables-responsive/dataTables.responsive.js')}}"></script>
    <script src="{{url('global/vendor/datatables-tabletools/dataTables.tableTools.js')}}"></script>
    <script src="{{url('global/vendor/matchheight/jquery.matchHeight-min.js')}}"></script>
    <script src="{{url('global/vendor/peity/jquery.peity.min.js')}}"></script>
    <script src="{{url('global/vendor/select2/select2.full.min.js')}}"></script>
    <script src="{{url('global/vendor/bootstrap-tokenfield/bootstrap-tokenfield.min.js')}}"></script>
    <script src="{{url('global/vendor/bootstrap-tagsinput/bootstrap-tagsinput.min.js')}}"></script>
    <script src="{{url('global/vendor/bootstrap-select/bootstrap-select.js')}}"></script>
    <script src="{{url('global/vendor/icheck/icheck.min.js')}}"></script>
    <script src="{{url('global/vendor/switchery/switchery.min.js')}}"></script>
    <script src="{{url('global/vendor/asrange/jquery-asRange.min.js')}}"></script>
    <script src="{{url('global/vendor/asspinner/jquery-asSpinner.min.js')}}"></script>
    <script src="{{url('global/vendor/clockpicker/bootstrap-clockpicker.min.js')}}"></script>
    <script src="{{url('global/vendor/ascolor/jquery-asColor.min.js')}}"></script>
    <script src="{{url('global/vendor/asgradient/jquery-asGradient.min.js')}}"></script>
    <script src="{{url('global/vendor/ascolorpicker/jquery-asColorPicker.min.js')}}"></script>
    <script src="{{url('global/vendor/bootstrap-maxlength/bootstrap-maxlength.js')}}"></script>
    <script src="{{url('global/vendor/jquery-knob/jquery.knob.js')}}"></script>
    <script src="{{url('global/vendor/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js')}}"></script>
    <script src="{{url('global/vendor/card/jquery.card.js')}}"></script>
    <script src="{{url('global/vendor/jquery-labelauty/jquery-labelauty.js')}}"></script>
    <script src="{{url('global/vendor/bootstrap-datepicker/bootstrap-datepicker.js')}}"></script>
    <script src="{{url('global/vendor/jt-timepicker/jquery.timepicker.min.js')}}"></script>
    <script src="{{url('global/vendor/datepair-js/datepair.min.js')}}"></script>
    <script src="{{url('global/vendor/datepair-js/jquery.datepair.min.js')}}"></script>
    <script src="{{url('global/vendor/jquery-strength/jquery-strength.min.js')}}"></script>
    <script src="{{url('global/vendor/multi-select/jquery.multi-select.js')}}"></script>
    <script src="{{url('global/vendor/typeahead-js/bloodhound.min.js')}}"></script>
    <script src="{{url('global/vendor/typeahead-js/typeahead.jquery.min.js')}}"></script>
    <script src="{{url('global/vendor/jquery-placeholder/jquery.placeholder.js')}}"></script>
    <!-- Scripts -->
    <script src="{{url('global/js/core.js')}}"></script>
    <script src="{{url('assets/js/site.js')}}"></script>
    <script src="{{url('assets/js/sections/menu.js')}}"></script>
    <script src="{{url('assets/js/sections/menubar.js')}}"></script>
    <script src="{{url('assets/js/sections/gridmenu.js')}}"></script>
    <script src="{{url('assets/js/sections/sidebar.js')}}"></script>
    <script src="{{url('global/js/configs/config-colors.js')}}"></script>
    <script src="{{url('assets/js/configs/config-tour.js')}}"></script>
    <script src="{{url('global/js/components/asscrollable.js')}}"></script>
    <script src="{{url('global/js/components/animsition.js')}}"></script>
    <script src="{{url('global/js/components/slidepanel.js')}}"></script>
    <script src="{{url('global/js/components/switchery.js')}}"></script>
    <script src="{{url('global/js/components/tabs.js')}}"></script>
    <script src="{{url('global/js/components/select2.js')}}"></script>
    <script src="{{url('global/js/components/bootstrap-tokenfield.js')}}"></script>
    <script src="{{url('global/js/components/bootstrap-tagsinput.js')}}"></script>
    <script src="{{url('global/js/components/bootstrap-select.js')}}"></script>
    <script src="{{url('global/js/components/icheck.js')}}"></script>
    <script src="{{url('global/js/components/switchery.js')}}"></script>
    <script src="{{url('global/js/components/asrange.js')}}"></script>
    <script src="{{url('global/js/components/asspinner.js')}}"></script>
    <script src="{{url('global/js/components/clockpicker.js')}}"></script>
    <script src="{{url('global/js/components/ascolorpicker.js')}}"></script>
    <script src="{{url('global/js/components/bootstrap-maxlength.js')}}"></script>
    <script src="{{url('global/js/components/jquery-knob.js')}}"></script>
    <script src="{{url('global/js/components/bootstrap-touchspin.js')}}"></script>
    <script src="{{url('global/js/components/card.js')}}"></script>
    <script src="{{url('global/js/components/jquery-labelauty.js')}}"></script>
    <script src="{{url('global/js/components/bootstrap-datepicker.js')}}"></script>
    <script src="{{url('global/js/components/jt-timepicker.js')}}"></script>
    <script src="{{url('global/js/components/datepair-js.js')}}"></script>
    <script src="{{url('global/js/components/jquery-strength.js')}}"></script>
    <script src="{{url('global/js/components/multi-select.js')}}"></script>
    <script>
    (function(document, window, $) {
      'use strict';
      var Site = window.Site;
      $(document).ready(function() {
        Site.run();
      });
    })(document, window, jQuery);
    </script>
    <script src="{{url('global/js/components/datatables.js')}}"></script>
    <script src="{{url('assets/examples/js/tables/datatable.js')}}"></script>
    <script src="{{url('global/js/components/matchheight.js')}}"></script>
    <script src="{{url('global/js/components/peity.js')}}"></script>
    <script src="{{url('assets/examples/js/forms/advanced.js')}}"></script>
  </body>
</html>