  <!-- Modal -->
  <div class="modal fade modal-fade-in-scale-up" id="confirm" tabindex="-1">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
          <h4 class="modal-title">Delete Confirmation</h4>
        </div>
        <div class="modal-body">
          <p>Are you sure you, want to delete?</p>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-warning" id="delete-btn">Delete</button>
          <button type="button" class="btn btn-default btn-pure margin-0" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <!-- End Modal -->