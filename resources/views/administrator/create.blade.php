@extends('layouts.master')
@section('content-backend')
    <div class="page-content">
      <!-- Panel Basic -->
      <div class="panel">
        <div class="panel-body container-fluid">
          
          {!! Form::open(['url' => ['admin/admin-management/add'], 'class' => 'container-fluid', 'method' => 'POST','enctype' => 'multipart/form-data']) !!}
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                    <h4 class="example-title">Name</h4>
                    <input type="text" class="form-control" name="name">
                </div>
                <div class="form-group">
                  <h4 class="example-title">Email</h4>
                  <div class="input-group">
                    <input type="email" class="form-control" name="email">
                    <span class="input-group-addon">@example.com</span>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <h4 class="example-title">Password</h4>
                  <input type="password" class="form-control" name="password">
                </div>
                <div class="form-group">
                  <h4 class="example-title">Confirm Password</h4>
                  <input type="password" class="form-control" name="cPassword">
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <button type="submit" class="form-control btn btn-primary">Save</button>
                </div>
              </div>
            {!! Form::close() !!}
           
        </div>
      </div>
      <!-- End Panel Basic -->
    </div>

    @endsection