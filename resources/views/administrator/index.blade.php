@extends('layouts.master')
@section('content-backend')
    <div class="page-content">
      <!-- Panel Basic -->
      <div class="panel">
        <header class="panel-heading">
          <div class="panel-actions">
            <?php if(Auth::user()->roles[0]->id < 3){?>
            <a  href="{{ url('admin/admin-management/add') }}">
              <i class="icon md-plus" aria-hidden="true"></i> Add more ...
            </a>
          <?php } ?>
          </div>
          <h3 class="panel-title">List Administrator</h3>
        </header>
        <div class="panel-body">
          <table class="table table-hover dataTable table-striped width-full" data-plugin="dataTable">
            <thead>
              <tr>
                <th>No </th>
                <th>Name</th>
                <th>Email</th>
                <th>Action</th>
              </tr>
            </thead>
            <tfoot>
              <tr>
                <th>No</th>
                <th>Name</th>
                <th>Email</th>
                <th>Action</th>
              </tr>
            </tfoot>
            <tbody>
              <tr>
                <?php $i=1;
                $data = Auth::user()->roles[0];?>
                @foreach( $users as $ur)
                <?php if(Auth::user()->roles[0]->id >= 3){?>
                @if ($ur->hasRole('user'))
                <td>{{$i}}</td>
                <td>{{ $ur->name }}</td>
                <td>{{ $ur->email }}</td>
                <td class="actions">
                  <!-- <button class="btn btn-sm btn-icon btn-pure btn-default" data-target="#userModal{{$ur->id}}" data-toggle="modal" type="button" data-original-title="Details"><i class="icon md-search" aria-hidden="true"></i></button> -->
                  <a href="{{ url('admin/admin-management/edit'.'/'. $ur->id) }}" class="btn btn-sm btn-icon btn-pure btn-default"
                  data-toggle="tooltip" data-original-title="Edit"><i class="icon md-edit" aria-hidden="true"></i></a>
                  <button class="btn btn-sm btn-icon btn-pure btn-default" data-target="#confirm{{$ur->id}}" data-toggle="modal" type="button" data-toggle="tooltip" data-original-title="Delete"><i class="icon md-close" aria-hidden="true"></i></button>
                </td>
                @endif

              <?php }else{ ?>
                <td>{{$i}}</td>
                <td>{{ $ur->name }}</td>
                <td>{{ $ur->email }}</td>
                <td class="actions">
                  <!-- <button class="btn btn-sm btn-icon btn-pure btn-default" data-target="#userModal{{$ur->id}}" data-toggle="modal" type="button" data-original-title="Details"><i class="icon md-search" aria-hidden="true"></i></button> -->
                  <a href="{{ url('admin/admin-management/edit'.'/'. $ur->id) }}" class="btn btn-sm btn-icon btn-pure btn-default"
                  data-toggle="tooltip" data-original-title="Edit"><i class="icon md-edit" aria-hidden="true"></i></a>
                  <button class="btn btn-sm btn-icon btn-pure btn-default" data-target="#confirm{{$ur->id}}" data-toggle="modal" type="button" data-toggle="tooltip" data-original-title="Delete"><i class="icon md-close" aria-hidden="true"></i></button>
                </td>
              <?php } ?>
              </tr>
              <?php $i++?>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
      <!-- End Panel Basic -->
    </div>
    @foreach($users as $ur)
    <!-- Modal -->
    <div class="modal fade modal-fade-in-scale-up" id="userModal{{$ur->id}}" aria-hidden="true"
    aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title">Details Admin</h4>
          </div>
          <div class="modal-body">
            <div class="form-group form-material">
              <label class="control-label" for="inputText">Name</label>
              <input type="text" value="{{ $ur->name }}" disabled class="form-control" id="inputText" name="inputText" placeholder="Text"
              />
            </div>
            <div class="form-group form-material">
              <label class="control-label" for="inputText">Email</label>
              <input type="text" value="{{ $ur->email }}" disabled class="form-control" id="inputText" name="inputText" placeholder="Text"
              />
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default btn-pure margin-0" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
    <!-- End Modal -->

    <!-- Modal -->
    <div class="modal fade modal-fade-in-scale-up" id="confirm{{$ur->id}}" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title">Delete Confirmation</h4>
          </div>
          <div class="modal-body">
            <p>Are you sure you, want to delete?</p>
          </div>
          <div class="modal-footer">
            <a href="{{url('admin/admin-management/delete/'.$ur->id)}}" class="btn btn-warning" id="delete-btn">Delete</a>
            <button type="button" class="btn btn-default btn-pure margin-0" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
    <!-- End Modal -->
    @endforeach

@endsection
