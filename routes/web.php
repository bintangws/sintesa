<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});
Route::get('/verifyemail/{token} ','Auth\RegisterController@verify');
Auth::routes();


Route::group(['prefix' => 'admin', 'middleware' => 'auth','isVerified'], function () {
	Route::get('/admin-management','AdminController@index');
    Route::get('/admin-management/create','AdminController@create');
    Route::post('/admin-management/create','AdminController@store');
    Route::get('/admin-management/edit/{id}','AdminController@edit');
    Route::post('/admin-management/edit/{id}','AdminController@update');
    Route::post('/admin-management/delete/{id}','AdminController@delete');

});
